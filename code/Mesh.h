#ifndef MESH_H
#define MESH_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <glm/glm.hpp>
#include "Shader.h"
#include "Texture.h"

#include "Camera.h"
#include "VAO.h"

class Mesh
{
public:
	Mesh(std::vector<Vertex> vertices, std::vector<uint32> indices, std::vector<Texture> textures);
	void Draw(Shader& shader, Camera& camera);

public:
	std::vector<uint32> GetIndices() const { return indices_; }
	void Cleanup();
public:
	VAO vao_;
	bool bNoTex_;

	std::vector<Vertex> vertices_;
	std::vector<uint32> indices_;
	std::vector<Texture> textures_;
	aiColor4D diffuseColor_;
	aiColor4D specularColor_;
};

#endif
