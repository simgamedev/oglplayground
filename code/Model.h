#ifndef MODEL_H
#define MODEL_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include "Mesh.h"


class Model
{
	friend Mesh;
	static int nextID;
public:
	Model(std::string filepath);
	void SetTransformation(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale);
	void Draw(Shader& shader, Camera& camera);
	void Cleanup();
protected:
	int id_;
	bool bNoTex_;
	std::string filepath_;
	std::string directory_;

	std::vector<Mesh> meshes_;
	std::vector<Texture> texturesLoaded_;
	// assimp
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
protected:
	glm::vec3 position_;
	glm::vec3 rotation_;
	glm::vec3 scale_;
	glm::mat4 modelMatrix_;
	void LoadFromFile(string filepath);
	std::vector<Texture> loadTextures(aiMaterial* mat, aiTextureType type);
};

#endif
