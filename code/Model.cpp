#include "Model.h"
#include "Utilities.h"
#include <glm/gtx/quaternion.hpp>



int Model::nextID = 0;
Model::Model(string filepath)
	: filepath_(filepath)
{
	id_ = nextID++;
	directory_ = filepath_.substr(0, filepath_.find_last_of('/'));
	LoadFromFile(filepath_);
}

void
Model::LoadFromFile(string filepath)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(filepath, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "Failed to load model at "	<< filepath_ << std::endl << importer.GetErrorString() << std::endl;
		return;
	}

	processNode(scene->mRootNode, scene);
}

void
Model::processNode(aiNode* node, const aiScene* scene)
{
	for (uint32 i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* aiMesh = scene->mMeshes[node->mMeshes[i]];
		Mesh newMesh = processMesh(aiMesh, scene);
		meshes_.push_back(newMesh);
	}

	// process all childen nodes
	for (uint32 i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}
}

Mesh
Model::processMesh(aiMesh* assimpMesh, const aiScene* scene)
{
	std::vector<Vertex> vertices;
	std::vector<uint32> indices;
	std::vector<Texture> textures;


	// ====================> LOAD VERTICES
	for (uint32 i = 0; i < assimpMesh->mNumVertices; i++)
	{
		Vertex vertex;
		// ===> POSITION
		vertex.position = glm::vec3(
			assimpMesh->mVertices[i].x,
			assimpMesh->mVertices[i].y,
			assimpMesh->mVertices[i].z
		);
		// ===< POSITIONs

		// ===> NORMAL
		vertex.normal = glm::vec3(
			assimpMesh->mNormals[i].x,
			assimpMesh->mNormals[i].y,
			assimpMesh->mNormals[i].z
		);
		// ===< NORMAL

		// ===> COLOR
		/*
		vertex.color = glm::vec3(
			assimpMesh->mColors[i].r,
			assimpMesh->mColors[i].g,
			assimpMesh->mColors[i].b
		);
		*/
		// ===< COLOR

		// ===> TEXTURE COORDS
		if (assimpMesh->mTextureCoords[0])
		{
			vertex.texCoord = glm::vec2(
				assimpMesh->mTextureCoords[0][i].x,
				assimpMesh->mTextureCoords[0][i].y
			);
		}
		else
		{
			vertex.texCoord = glm::vec2(0.0f);
		}
		// ===< TEXTURE COORDS
		vertices.push_back(vertex);
	}
	// ====================< LOAD VERTICES


	// ====================> LOAD INDICES
	for (uint32 i = 0; i < assimpMesh->mNumFaces; i++)
	{
		aiFace face = assimpMesh->mFaces[i];
		for (uint32 j = 0; j < face.mNumIndices; j++)
		{
			indices.push_back(face.mIndices[j]);
		};
	}
	// ====================< LOAD INDICES

	// ====================> LOAD TEXTURES/MATERIALS
	if (assimpMesh->mMaterialIndex >= 0)
	{
		aiMaterial* aiMat = scene->mMaterials[assimpMesh->mMaterialIndex];
		// diffuse maps
		std::vector<Texture> diffuseMaps = loadTextures(aiMat, aiTextureType_DIFFUSE);
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

		// specular maps
		std::vector<Texture> specularMaps = loadTextures(aiMat, aiTextureType_SPECULAR);
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}
	// ====================< LOAD TEXTURES/MATERIALS

	return Mesh(vertices, indices, textures);
}

std::vector<Texture>
Model::loadTextures(aiMaterial* mat, aiTextureType type)
{
	std::vector<Texture> textures;

	for (uint32 i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString texFilename;
		mat->GetTexture(type, i, &texFilename);
		std::cout << filepath_ << "/" << texFilename.C_Str() << std::endl;
		
		// prevent duplicates
		bool skip = false;
		for (uint32 j = 0; j < texturesLoaded_.size(); j++)
		{
			if (std::strcmp(texturesLoaded_[j].GetFilename().data(), texFilename.C_Str()) == 0)
			{
				textures.push_back(texturesLoaded_[j]);
				skip = true;
				break;
			}
		}

		if (!skip)
		{
			Texture tex(directory_ + "/" + std::string(texFilename.C_Str()), type);
			tex.Load(false);
			textures.push_back(tex);
			texturesLoaded_.push_back(tex);
		}
	}

	return textures;
}


void
Model::SetTransformation(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale)
{
	position_ = position;
	rotation_ = rotation;
	scale_ = scale;

	// ==========> MODEL MATRIX
	glm::mat4 transMatrix = glm::translate(glm::mat4(1.0f), position_);
	glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), scale_);
	// ===> QUATERNION WAY
	glm::vec3 eulerInRadians(glm::radians(rotation.x),
							 glm::radians(rotation.y),
							 glm::radians(rotation.z));
	glm::quat quatRot(eulerInRadians);
	glm::mat4 rotMatrix = glm::toMat4(quatRot);
	// NOTE: order must be SRT(glsl:MVP)(backward)
	modelMatrix_ = transMatrix * rotMatrix * scaleMatrix;
	// ==========< MODEL MATRIX
}


void
Model::Draw(Shader& shader, Camera& camera)
{
	shader.Use();
	shader.SetMat4("model", modelMatrix_);
	for (uint32 i = 0, numMeshes = meshes_.size(); i < numMeshes; i++)
	{
		meshes_[i].Draw(shader, camera);
	}
}

void
Model::Cleanup()
{
}
