#ifndef CAMERA_H
#define CAMERA_H

#include "Shader.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class Camera
{
public:
	Camera(int scrWidth, int scrHeight, glm::vec3 position);
	void CalcMatrix(float FOVdeg, float nearPlane, float farPlane);
	void Inputs(GLFWwindow* window);
public:
	glm::mat4 cameraMatrix;
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 worldUp;

	int scrWidth;
	int scrHeight;
	float speed;
	float sensitivity;
	bool firstClick;
	
};
#endif
