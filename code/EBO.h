#ifndef EBO_H
#define EBO_H

#include <glad/glad.h>
#include <vector>

class EBO
{
public:
	EBO(std::vector<GLuint>& indices);
	//EBO(GLuint* indices, GLsizeiptr size);
	void Bind();
	void UnBind();
	void Delete();
public:
	GLuint ID;
};

#endif
