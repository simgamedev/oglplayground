#include "Mesh.h"
#include "VBO.h"
#include "EBO.h"

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<uint32> indices, std::vector<Texture> textures)
	: bNoTex_(false)
{
	vertices_ = vertices;
	indices_ = indices;
	textures_ = textures;

	vao_.Bind();
	VBO vbo(vertices);
	EBO ebo(indices);
	// position
	vao_.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*)0);
	// normal
	vao_.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*)(3 * sizeof(float)));
	// color
	vao_.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*)(6 * sizeof(float)));
	// texCoord
	vao_.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*)(9 * sizeof(float)));
	// Unbind all to prevent accidentally modifying them
	vao_.UnBind();
	vbo.UnBind();
	ebo.UnBind();
}


void
Mesh::Draw(Shader& shader, Camera& camera)
{
	shader.Use();
	// ==========> CASE: NO TEXTURE
	if (bNoTex_)
	{
		shader.Set4Float("materialSimple.diffuse", diffuseColor_);
		shader.Set4Float("materialSimple.specular", specularColor_);
		shader.SetBool("bNoTex", true);
	}
	// ==========< CASE: NO TEXTURE
	else
	{
		// ===> GLTF EMBEDED TEXTUERS(We assume that gltf models always have embedded textures)
		shader.SetBool("bNoTex", false);
		uint32 diffuseIdx = 0;
		uint32 specularIdx = 0;
		uint32 normalIdx = 0;
		// textures
		for (uint32 i = 0; i < textures_.size(); i++)
		{
			glActiveTexture(GL_TEXTURE0 + i);

			string name;
			switch (textures_[i].GetType())
			{
				case aiTextureType_DIFFUSE:
				{
					name = "diffuse" + std::to_string(diffuseIdx++);
				} break;

				case aiTextureType_SPECULAR:
				{
					name = "specular" + std::to_string(specularIdx++);
				} break;

				case aiTextureType_NORMALS:
				{
					name = "normal" + std::to_string(normalIdx++);
				} break;

				default:
				{
				} break;
			}

			shader.SetInt(name, i);
			textures_[i].Bind();
		}
	}

	shader.SetMat4("cameraMatrix", camera.cameraMatrix);
	shader.Set3Float("cameraPos", camera.position);
	vao_.Bind();
	glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, 0);
	vao_.UnBind();

	glActiveTexture(GL_TEXTURE0);
}


void
Mesh::Cleanup()
{
	//vao_.Cleanup();
}
