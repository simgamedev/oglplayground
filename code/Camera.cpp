#include "Camera.h"
#include<glm/gtx/rotate_vector.hpp>
#include<glm/gtx/vector_angle.hpp>

Camera::Camera(int scrWidth, int scrHeight, glm::vec3 position)
	: scrWidth(scrWidth)
	, scrHeight(scrHeight)
	, position(position)
	, front(glm::vec3(0.0f, 0.0f, -1.0f))
	, worldUp(glm::vec3(0.0f, 1.0f, 0.0f))
	, speed(0.05f)
	, sensitivity(50.0f)
	, firstClick(true)
{
}

void
Camera::CalcMatrix(float FOVdeg, float nearPlane, float farPlane)
{
	glm::mat4 view = glm::mat4(1.0f);
	glm::mat4 projection = glm::mat4(1.0f);

	view = glm::lookAt(position, position + front, worldUp);
	projection = glm::perspective(glm::radians(FOVdeg), (float)(scrWidth/scrHeight), nearPlane, farPlane);
	cameraMatrix = projection * view;
}


void
Camera::Inputs(GLFWwindow* window)
{
	// WASD
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		position += speed * front;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		position -= speed * glm::normalize(glm::cross(front, worldUp));
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		position -= speed * front;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		position += speed * glm::normalize(glm::cross(front, worldUp));
	}
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
	{
		position -= speed * worldUp;
	}
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
	{
		position += speed * worldUp;
	}
	// SHIFT speed up
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		speed = 0.3f;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
	{
		speed = 0.05f;
	}

	// Mouse right fly through mode
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

		if (firstClick)
		{
			glfwSetCursorPos(window, (scrWidth / 2), (scrHeight / 2));
			firstClick = false;
		}

		double mouseX;
		double mouseY;
		glfwGetCursorPos(window, &mouseX, &mouseY);

		float rotX = sensitivity * (float)(mouseY - (scrHeight / 2)) / scrHeight;
		float rotY = sensitivity * (float)(mouseX - (scrWidth / 2)) / scrWidth;

		glm::vec3 newFront = glm::rotate(front, glm::radians(-rotX), glm::normalize(glm::cross(front, worldUp)));
		if (abs(glm::angle(newFront, worldUp) - glm::radians(90.0f)) <= glm::radians(85.0f))
		{
			front = newFront;
		}

		front = glm::rotate(front, glm::radians(-rotY), worldUp);
		glfwSetCursorPos(window, (scrWidth / 2), (scrHeight / 2));
	}
	else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		firstClick = true;
	}

}
