#pragma once
#include <windows.h>
#include <Shlwapi.h>

namespace Utils
{
	inline string GetAssetsDirectory()
	{
		HMODULE hModule = GetModuleHandle(nullptr);
		if(hModule)
		{
			char path[256];
			GetModuleFileNameA(hModule, path, sizeof(path));
			PathRemoveFileSpecA(path);
			strcat_s(path, "\\");
		    string result(path);
			std::size_t pos = result.find("build");
			result = result.substr(0, pos);
			result = result + "assets\\";
			return(result);
		}
		return "";
	}
}
