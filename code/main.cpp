#include <iostream>
#include "common.h"
#include "utilities.h"

#include "Mesh.h"
#include "Model.h"

#define SCR_WIDTH 800
#define SCR_HEIGHT 800
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
bool bHasSpecularMap = true;
Shader* flatBlinnPhong;

// Vertices coordinates
Vertex vertices[] =
{ //               COORDINATES           /            COLORS          /           TexCoord         /       NORMALS         //
	Vertex{glm::vec3(-1.0f, 0.0f,  1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(0.0f, 0.0f)},
	Vertex{glm::vec3(-1.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(0.0f, 1.0f)},
	Vertex{glm::vec3(1.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(1.0f, 1.0f)},
	Vertex{glm::vec3(1.0f, 0.0f,  1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(1.0f, 0.0f)}
};

// Indices for vertices order
GLuint indices[] =
{
	0, 1, 2,
	0, 2, 3
};

Vertex lightVertices[] =
{ //     COORDINATES     //
	Vertex{glm::vec3(-0.1f, -0.1f,  0.1f)},
	Vertex{glm::vec3(-0.1f, -0.1f, -0.1f)},
	Vertex{glm::vec3(0.1f, -0.1f, -0.1f)},
	Vertex{glm::vec3(0.1f, -0.1f,  0.1f)},
	Vertex{glm::vec3(-0.1f,  0.1f,  0.1f)},
	Vertex{glm::vec3(-0.1f,  0.1f, -0.1f)},
	Vertex{glm::vec3(0.1f,  0.1f, -0.1f)},
	Vertex{glm::vec3(0.1f,  0.1f,  0.1f)}
};

GLuint lightIndices[] =
{
	0, 1, 2,
	0, 2, 3,
	0, 4, 7,
	0, 7, 3,
	3, 7, 6,
	3, 6, 2,
	2, 6, 5,
	2, 5, 1,
	1, 5, 4,
	1, 4, 0,
	4, 5, 6,
	4, 6, 7
};

int main()
{
	// ===> INIT OPENGL
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GL_DEPTH_BUFFER_BIT, 32);
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "RelearnOpenGL", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
	}
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);
	gladLoadGL();
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS); // NOTE: glDepthFunc by default is GL_LESS
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// ===< INIT OPENGL


	// ===> LOAD SHADERS
	flatBlinnPhong = new Shader("flatBlinnPhong", 
								Utils::GetAssetsDirectory() + "shaders/flat_blinn_phong.vert",
								Utils::GetAssetsDirectory() + "shaders/flat_blinn_phong.frag");
	Shader glassUnlitShader("glassUnlit",
						    Utils::GetAssetsDirectory() + "shaders/flat_blinn_phong.vert",
						    Utils::GetAssetsDirectory() + "shaders/glass_unlit.frag");
	Shader lightGizmoShader("lightGizmo",
							Utils::GetAssetsDirectory() + "shaders/flat_blinn_phong.vert",
							Utils::GetAssetsDirectory() + "shaders/lightGizmo.frag");
	// ===< LOAD SHADERS

	// ===> LOAD MESHES
	// windowModel
	Model windowModel(Utils::GetAssetsDirectory() + "models/windows/scene.gltf");
	windowModel.SetTransformation(glm::vec3(0.0f, -0.9f, 0.0f),
								  glm::vec3(0.0f, -90.0f, 0.0f),
								  glm::vec3(0.5f));
	// floor
	Texture floorTextures[]
	{
		Texture(Utils::GetAssetsDirectory() + "Textures/planks.png", aiTextureType_DIFFUSE),
		Texture(Utils::GetAssetsDirectory() + "Textures/planksSpec.png", aiTextureType_SPECULAR),
	};
	for (auto tex : floorTextures)
		tex.Load();
	std::vector<Vertex> verts(vertices, vertices + sizeof(vertices) / sizeof(Vertex));
	std::vector<GLuint> idxs(indices, indices + sizeof(indices) / sizeof(GLuint));
	std::vector<Texture> texs(floorTextures, floorTextures + sizeof(floorTextures) / sizeof(Texture));
	Mesh floor(verts, idxs, texs);
	// light
	std::vector<Vertex> lightVerts(lightVertices, lightVertices + sizeof(lightVertices) / sizeof(Vertex));
	std::vector<GLuint> lightIdxs(lightIndices, lightIndices + sizeof(lightIndices) / sizeof(GLuint));
	Mesh lighCube(lightVerts, lightIdxs, texs);
	// ===< load texture
	Model bottle(Utils::GetAssetsDirectory() + "Models/bottle/scene.gltf");
	bottle.SetTransformation(glm::vec3(0.0f, 0.0f, -1.0f),
							   glm::vec3(-90.0f, 0.0f, 0.0f),
							   glm::vec3(0.1f));
	// ===< LOAD MESHES



	// ===> camera
	Camera camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0f, 0.0f, 2.0f));
	// ===< camera



	glm::vec4 lightColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	glm::vec3 lightPos = glm::vec3(0.5f, 1.0f, 0.5f);
	glm::mat4 lightModel = glm::mat4(1.0f);
	lightModel = glm::translate(lightModel, lightPos);

	glm::vec3 objectPos = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::mat4 objectModel = glm::mat4(1.0f);
	objectModel = glm::translate(objectModel, objectPos);

	flatBlinnPhong->Use();
	flatBlinnPhong->Set4Float("lightColor", lightColor);
	flatBlinnPhong->Set3Float("lightPos", lightPos);
	flatBlinnPhong->SetMat4("model", objectModel);

	lightGizmoShader.Use();
	lightGizmoShader.Set4Float("lightColor", lightColor);
	lightGizmoShader.SetMat4("model", lightModel);


	double prevTime = 0.0f;
	double currentTime = 0.0f;
	double deltaTime = 0.0f;
	int numFrames = 0;
	// test fps diff with/without face culling
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	//glFrontFace(GL_CCW);

	// disable vsync
	glfwSwapInterval(0);
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	while (!glfwWindowShouldClose(window))
	{
		// ===> calc FPS
		currentTime = glfwGetTime();
		deltaTime = currentTime - prevTime;
		numFrames++;
		if (deltaTime >= 1.0f / 60.0f)
		{
			std::string FPS = std::to_string((1.0f / deltaTime) * numFrames);
			std::string frameTime = std::to_string((deltaTime / numFrames) * 1000);
			std::string windowTitle = "OGLPlayground - " + FPS + "FPS / " + frameTime + "ms";
			glfwSetWindowTitle(window, windowTitle.c_str());
			prevTime = currentTime;
			numFrames = 0;

			glfwPollEvents();
			camera.Inputs(window);
			camera.CalcMatrix(45.0f, 0.1f, 100.0f);
		}
		// ===< calc FPS

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		flatBlinnPhong->Use();
		flatBlinnPhong->SetMat4("model", objectModel);
		floor.Draw(*flatBlinnPhong, camera);
		glEnable(GL_BLEND);
		bottle.Draw(*flatBlinnPhong, camera);
		bottle.Draw(*flatBlinnPhong, camera);
		windowModel.Draw(*flatBlinnPhong, camera);
		glDisable(GL_BLEND);


		// ===> stencil buffer reset
		glStencilMask(0xFF);
		glStencilFunc(GL_ALWAYS, 0, 0xFF);
		glEnable(GL_DEPTH_TEST);
		// ===< stencil buffer reset

		// light gizmo
		lighCube.Draw(lightGizmoShader, camera);

		glfwSwapBuffers(window);
	}


	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}	

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
	{
		std::cout << "key 1 pressed" << std::endl;
		bHasSpecularMap = !bHasSpecularMap;
		flatBlinnPhong->Use();
		flatBlinnPhong->SetBool("bHasSpecular", bHasSpecularMap);
	}
}
