#version 330 core

out vec4 FragColor;

// depth buffer in OpenGL is not linear, so in order to 'see' the depth buffer, we convert it back to linear
float near = 0.1f;
float far = 100.0f;

float linearizeDepth(float depth)
{
	return (2.0 * near * far) / (far + near - (depth * 2.0 - 1.0) * (far - near));
}

float logisticDepth(float depth, float steepness = 0.5f, float offset = 5.0f)
{
	float zVal = linearizeDepth(depth);
	return (1 / (1 + exp(-steepness * (zVal - offset))));
}

void main()
{
	FragColor = vec4(vec3(linearizeDepth(gl_FragCoord.z)), 1.0f);
}
