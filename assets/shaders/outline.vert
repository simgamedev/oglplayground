#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

uniform mat4 cameraMatrix;
uniform mat4 model;
uniform float outlineScale;
void main()
{
	vec3 VertexPos = vec3(model * vec4(aPos + aNormal * outlineScale, 1.0f));

	gl_Position = cameraMatrix * vec4(VertexPos, 1.0f);
}
