#version 330 core

out vec4 FragColor;

in vec2 TexCoord;
in vec3 VertexNormal;
in vec3 VertexPos;

uniform sampler2D diffuse0;
uniform sampler2D specular0;
uniform vec3 lightPos;
uniform vec4 lightColor;
uniform vec3 cameraPos;
uniform bool bHasSpecular = true;

void main()
{
	vec3 vertexNormal = normalize(VertexNormal);
	vec3 lightDir = normalize(lightPos - VertexPos);

	float ambientStr = 0.2f;
	float diffuseStr = max(abs(dot(vertexNormal, lightDir)), 0.0f); // absolute helps with grass shading
	float specularK = 0.8f;
	vec3 viewDir = normalize(cameraPos - VertexPos);
	vec3 reflectionDir = reflect(-lightDir, VertexNormal);
	//float specularStr = pow(max(dot(viewDir, reflectionDir), 0.0f), 8) * specularK; // PHONG
	// use blinn phong 
	vec3 halfWay = normalize(viewDir + lightDir);
	float specularStr = pow(max(dot(VertexNormal, halfWay), 0.0f), 8) * specularK;

	// grass transparent
	if(texture(diffuse0, TexCoord).a < 0.1)
		discard;
	// has specular map
	if(bHasSpecular)
		FragColor = texture(diffuse0, TexCoord) * lightColor * (diffuseStr + ambientStr) + texture(specular0, TexCoord).r * specularStr;
	else
	// no specular map
		FragColor = texture(diffuse0, TexCoord) * lightColor * (diffuseStr + ambientStr + specularStr);
}
