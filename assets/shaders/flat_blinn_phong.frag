#version 330 core

out vec4 FragColor;

in vec2 TexCoord;
in vec3 VertexNormal;
in vec3 VertexPos;

uniform sampler2D diffuse0;
uniform sampler2D specular0;
uniform vec3 lightPos;
uniform vec4 lightColor;
uniform vec3 cameraPos;
uniform bool bHasSpecular = true;

vec4 calc_dir_light()
{
	float intensity = 0.3f;

	vec3 lightVec = vec3(1.0f, 1.0f, 0.0f);
	vec3 vertexNormal = normalize(VertexNormal);
	vec3 lightDir = normalize(lightVec);

	float ambientStr = 0.2f;
	float diffuseStr = max(dot(vertexNormal, lightDir), 0.0f);
	float specularK = 0.8f;
	vec3 viewDir = normalize(cameraPos - VertexPos);
	vec3 reflectionDir = reflect(-lightDir, VertexNormal);
	float specularStr = pow(max(dot(viewDir, reflectionDir), 0.0f), 8) * specularK;
	// has specular map
	if(bHasSpecular)
		return (texture(diffuse0, TexCoord) * lightColor * (diffuseStr  + ambientStr) * intensity + texture(specular0, TexCoord).r * specularStr * intensity);
	else
		// no specular map
		return (FragColor = texture(diffuse0, TexCoord) * lightColor * (diffuseStr + ambientStr + specularStr) * intensity);
}
vec4 calc_point_light()
{
	// ===> attenuation
	vec3 lightVec = lightPos - VertexPos;
	float dist = length(lightVec);
	float a = 1.0;
	float b = 0.04;
	float attenuation = 1.0f / (a * dist * dist + b * dist + 1.0f);
	// ===<

	vec3 vertexNormal = normalize(VertexNormal);
	vec3 lightDir = normalize(lightVec);

	float ambientStr = 0.2f;
	float diffuseStr = max(dot(vertexNormal, lightDir), 0.0f);
	float specularK = 0.8f;
	vec3 viewDir = normalize(cameraPos - VertexPos);
	vec3 reflectionDir = reflect(-lightDir, VertexNormal);
	float specularStr = pow(max(dot(viewDir, reflectionDir), 0.0f), 8) * specularK;
	// has specular map
	if(bHasSpecular)
		return (texture(diffuse0, TexCoord) * lightColor * (diffuseStr * attenuation + ambientStr) + texture(specular0, TexCoord).r * specularStr * attenuation);
	else
		// no specular map
		return (FragColor = texture(diffuse0, TexCoord) * lightColor * (diffuseStr * attenuation + ambientStr + specularStr * attenuation));
}

void main()
{
	FragColor = calc_dir_light() + calc_point_light();
}
