#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aColor;
layout (location = 3) in vec2 aTexCoord;

uniform mat4 cameraMatrix;
uniform mat4 model;

out vec3 VertexPos;
out vec3 VertexNormal;
out vec2 TexCoord;

void main()
{
	VertexPos = vec3(model * vec4(aPos, 1.0f));
	gl_Position = cameraMatrix * vec4(VertexPos, 1.0f);


	// ===> OUT
	TexCoord = aTexCoord;
	VertexNormal = aNormal;
	// ===< OUT
}
